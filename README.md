Installation:

1. `go get bitbucket.org/whjms/msgr`
2. Create `~/.config/msgr/config.json`:

        {
            "AccessToken": "YOUR_FB_API_ACCESS_TOKEN",
            "Username": "YOUR_MESSENGER_USERNAME",
            "Password": "YOUR_MESSENGER_PASSWORD"
        }

3. `go install bitbucket.org/whjms/msgr`
4. `msgr`
