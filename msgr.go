package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"text/tabwriter"
	"time"

	fb "github.com/huandu/facebook"
	"github.com/jroimartin/gocui"
	"github.com/fatih/color"
	"github.com/unixpickle/fbmsgr"
)

type Config struct {
	AccessToken string
	Username    string
	Password    string
}

const (
	nameMessageWindow = "messages"
	nameInput         = "input"
	nameThreadList    = "threads"
	stateChooseThread = iota
	stateInThread     = iota
)

var nameCache = make(map[string]string)
var session *fbmsgr.Session
var config Config
var state int
var threads []*fbmsgr.ThreadInfo
var currThread *fbmsgr.ThreadInfo

func getName(fbID *string, fullName bool) string {
	if name, ok := nameCache[*fbID]; ok {
		return name
	}

	var field string
	if fullName {
		field = "name"
	} else {
		field = "first_name"
	}

	res, _ := fb.Get(fmt.Sprintf("/%s", *fbID), fb.Params{
		"fields":       field,
		"access_token": config.AccessToken,
	})

	var name string
	res.DecodeField(field, &name)
	nameCache[*fbID] = name
	return name
}

func isGroupThread(t *fbmsgr.ThreadInfo) bool {
	return t.OtherUserFBID == nil
}

func subscribe(g *gocui.Gui) {
	for {
		x, err := session.ReadEvent()
		if err != nil {
			panic(err)
		}

		if msg, ok := x.(fbmsgr.MessageEvent); ok {
			if msg.OtherUser != "" && msg.OtherUser != currThread.ThreadFBID {
				continue
			} else if msg.GroupThread != "" && msg.GroupThread != currThread.ThreadFBID {
				continue
			}

			showMessage(&msg, g)
			session.SendReadReceipt(msg.MessageID)
		}
	}
}

func getThreads(sess *fbmsgr.Session, max int) (*fbmsgr.ThreadListResult, error) {
	res, err := sess.Threads(0, max)
	if err != nil {
		return nil, err
	}

	// fill in thread names for 1-1 chats
	for _, t := range res.Threads {
		if t.OtherUserFBID != nil {
			t.Name = getName(t.OtherUserFBID, true)
		}
	}
	return res, nil
}

func showMessage(msg *fbmsgr.MessageEvent, g *gocui.Gui) {
	g.Execute(func(g *gocui.Gui) error {
		v, err := g.View(nameMessageWindow)
		if err != nil {
			return err
		}

		col := color.New(color.FgMagenta)
		name := getName(&msg.SenderFBID, false)
		fmt.Fprintf(v, "%s %s %s\n",
			time.Now().Format("15:04"),
			col.Sprintf("<%s>", name),
			msg.Body,
		)

		return nil
	})
}

func quit(g *gocui.Gui, v *gocui.View) error {
	return gocui.ErrQuit
}

func layout(g *gocui.Gui) error {
	maxX, maxY := g.Size()

	switch state {
	case stateInThread:
		if v, err := g.SetView(nameMessageWindow, 0, 0, maxX-1, maxY-5); err != nil {
			if err != gocui.ErrUnknownView {
				return err
			}
			v.Wrap = true
			v.Autoscroll = true
			v.Title = currThread.Name
		}

		if v, err := g.SetView(nameInput, 0, maxY-4, maxX-1, maxY-1); err != nil {
			if err != gocui.ErrUnknownView {
				return err
			}
			if _, err := g.SetCurrentView(nameInput); err != nil {
				return err
			}

			v.Editable = true
			v.Wrap = true
		}
	case stateChooseThread:
		if v, err := g.SetView(nameThreadList, 0, 0, maxX-1, maxY-1); err != nil {
			if err != gocui.ErrUnknownView {
				return err
			}
			if _, err := g.SetCurrentView(nameThreadList); err != nil {
				return err
			}
			v.Wrap = true
			v.Autoscroll = true
			v.Highlight = true
			v.SelBgColor = gocui.ColorGreen
			v.SelFgColor = gocui.ColorBlack
		}

	}

	return nil
}

func initKeybindings(g *gocui.Gui) error {
	if err := g.SetKeybinding("", gocui.KeyCtrlC, gocui.ModNone, quit); err != nil {
		log.Panicln(err)
	}

	if err := g.SetKeybinding(nameInput, gocui.KeyEnter, gocui.ModNone, sendMessage); err != nil {
		return err
	}

	if err := g.SetKeybinding(nameInput, gocui.KeyArrowUp, gocui.ModNone,
		func(g *gocui.Gui, v *gocui.View) error {
			messageView, _ := g.View(nameMessageWindow)
			scrollView(g, messageView, -1)
			return nil
		}); err != nil {
		return err
	}
	if err := g.SetKeybinding(nameInput, gocui.KeyArrowDown, gocui.ModNone,
		func(g *gocui.Gui, v *gocui.View) error {
			messageView, _ := g.View(nameMessageWindow)
			scrollView(g, messageView, 1)
			return nil
		}); err != nil {
		return err
	}

	if err := g.SetKeybinding(nameThreadList, gocui.KeyArrowDown, gocui.ModNone, threadDown); err != nil {
		return err
	}
	if err := g.SetKeybinding(nameThreadList, gocui.KeyArrowUp, gocui.ModNone, threadUp); err != nil {
		return err
	}
	if err := g.SetKeybinding(nameThreadList, gocui.KeyEnter, gocui.ModNone, showThread); err != nil {
		return err
	}
	return nil
}

func sendMessage(g *gocui.Gui, v *gocui.View) error {
	// janky -- we end up stripping legit newlines here. see if there's
	// a way to get direct access to each line and concat them without
	// adding newlines like Buffer() does
	msg := strings.Replace(v.Buffer(), "\n", "", -1)
	if isGroupThread(currThread) {
		session.SendGroupText(currThread.ThreadFBID, msg)
	} else {
		session.SendText(currThread.ThreadFBID, msg)
	}
	v.Clear()
	v.SetCursor(0, 0)
	return nil
}

func showThread(g *gocui.Gui, v *gocui.View) error {
	_, threadIdx := v.Cursor()
	currThread = threads[threadIdx]
	state = stateInThread
	go subscribe(g)
	return nil
}

func threadDown(g *gocui.Gui, v *gocui.View) error {
	if v != nil {
		cx, cy := v.Cursor()
		if err := v.SetCursor(cx, cy+1); err != nil {
			ox, oy := v.Origin()
			if err := v.SetOrigin(ox, oy+1); err != nil {
				return err
			}
		}
	}
	return nil
}

func threadUp(g *gocui.Gui, v *gocui.View) error {
	if v != nil {
		cx, cy := v.Cursor()
		if err := v.SetCursor(cx, cy-1); err != nil {
			ox, oy := v.Origin()
			if err := v.SetOrigin(ox, oy-1); err != nil && oy > 0 {
				return err
			}
		}
	}
	return nil
}

func numMessagesReceived(g *gocui.Gui) int {
	messages, _ := g.View(nameMessageWindow)
	return strings.Count(messages.ViewBuffer(), "\n")
}

func scrollView(g *gocui.Gui, v *gocui.View, dy int) {
	if v != nil {
		_, y := v.Size()
		ox, oy := v.Origin()
		// if at bottom, re-enable autoscroll
		if oy+dy > numMessagesReceived(g)-y-1 {
			v.Autoscroll = true
		} else {
			v.Autoscroll = false
			v.SetOrigin(ox, oy+dy)
		}
	}
}

func outputThreads(out io.Writer, threads []*fbmsgr.ThreadInfo) {
	w := new(tabwriter.Writer)
	w.Init(out, 0, 8, 1, '\t', 0)
	for _, t := range threads {
		threadTime := time.Unix(int64(t.ServerTimestamp), 0).Format("Mon 15:04")
		line := fmt.Sprintf("[%d]\t%s\t%s",
			t.UnreadCount,
			threadTime,
			t.Name)
		fmt.Fprintln(w, line)

	}
	fmt.Fprintln(w)
	w.Flush()
}

func showThreadList(g *gocui.Gui) error {
	g.Execute(func(g *gocui.Gui) error {
		threadView, err := g.View(nameThreadList)
		if err != nil {
			return err
		}

		res, err := getThreads(session, 10)
		if err != nil {
			return err
		}
		threads = res.Threads

		outputThreads(threadView, threads)
		return nil
	})
	return nil
}

func getConfig() (Config, error) {
	var config Config

	home := os.Getenv("HOME")
	f, err := os.Open(fmt.Sprintf("%s/.config/msgr/config.json", home))
	if err != nil {
		return config, err
	}

	decoder := json.NewDecoder(f)
	config = Config{}
	err = decoder.Decode(&config)
	if err != nil {
		return config, err
	}

	return config, nil
}

func main() {
	c, err := getConfig()
	config = c
	if err != nil {
		fmt.Println("Error reading config: ", err)
		os.Exit(1)
	}

	fmt.Printf("Authenticating...")
	if config.Username == "" || config.Password == "" || config.AccessToken == "" {
		fmt.Println("error: empty username/pass/access token, exiting")
		os.Exit(1)
	}

	sess, err := fbmsgr.Auth(config.Username, config.Password)
	if err != nil {
		panic(err)
	} else {
		session = sess
	}
	defer session.Close()
	fmt.Println("done")

	state = stateChooseThread

	g, err := gocui.NewGui(gocui.OutputNormal)
	if err != nil {
		log.Panicln(err)
	}
	defer g.Close()
	g.Cursor = true
	g.SetManagerFunc(layout)

	if err := initKeybindings(g); err != nil {
		panic(err)
	}

	if err := showThreadList(g); err != nil {
		panic(err)
	}

	// go subscribe(g)

	if err := g.MainLoop(); err != nil && err != gocui.ErrQuit {
		panic(err)
	}
}
